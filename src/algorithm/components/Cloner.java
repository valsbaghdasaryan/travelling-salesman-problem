package algorithm.components;

import java.util.ArrayList;

/**
 * Created by Vardan on 07/12/2016.
 */
public class Cloner {
    public static ArrayList<Vertex> cloneVertices(ArrayList<Vertex> current)
    {
        ArrayList<Vertex> cloned = new ArrayList<>(current.size());
        cloned.addAll(current);
        return cloned;
    }

    public static ArrayList<Vehicle> cloneVehicleArray(ArrayList<Vehicle> vehicleArrayList)
    {
        ArrayList<Vehicle> cloned = new ArrayList<>(vehicleArrayList.size());
        for(Vehicle v : vehicleArrayList)
        {
            Vehicle vn = new Vehicle(v.getClonedVertices());
            cloned.add(vn);
        }
        return cloned;
    }
}
