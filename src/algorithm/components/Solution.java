package algorithm.components;

import algorithm.MetaHeuristics;

import java.util.ArrayList;

/**
 * Created by Vardan on 05/12/2016.
 */
public class Solution {
    public ArrayList<Vehicle> vehicles;

    public Solution(ArrayList<Vehicle> vehicles )
    {
        this.vehicles = vehicles;
    }


    public  ArrayList<Vehicle> getVehicles()
    {
        return this.vehicles;
    }

    public ArrayList<Vehicle> getVehiclesCloned()
    {
        return Cloner.cloneVehicleArray(this.vehicles);
    }



    /**
     * This method compares argument solution with itself, and if his solution is better he returns 'true'
     * @param solutionToCompare
     * @return
     */
    public boolean isBetterThan(Solution solutionToCompare)
    {
        if(this.getDistance() < solutionToCompare.getDistance())
        {
            return true;
        }
        return false;
    }

    public float getDistance() {
        float distance = 0;
        for(Vehicle r : vehicles)
        {
            distance += r.getDistance();
        }
        return distance;
    }

    /**
     * This method check if the solution is good enough to stop te measurement ( if we reach the local maximum )
     * @return true if solution meet the expectation of the problem
     */
    public boolean isAcceptableSolution()
    {
        return  false;// TODO need to implement code
    }

    public void print()
    {
        for(Vehicle v : this.vehicles)
        {
            v.print();
        }
        System.out.println("Distance of this solution : " + getDistance());
    }
}
