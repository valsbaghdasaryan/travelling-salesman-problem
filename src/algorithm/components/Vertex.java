package algorithm.components;

/**
 * Created by Vardan on 30/11/2016.
 */
public class Vertex {
    private int id;
    private float x;
    private float y;
    private float quantity;
    private float timeMin;
    private float timeMax;
    private float duration;

    public Vertex(int id, float x, float y, float quantity, float timeMin, float timeMax, float duration)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.quantity = quantity;
        this.timeMin = timeMin;
        this.timeMax = timeMax;
        this.duration = duration;
    }

    public void print()
    {
        System.out.println("id: " + this.id + " (" + this.x+ " : " + this.y + ")");
    }

    public float getDistanceFrom(Vertex v)
    {
        float distance = (float)Math.sqrt(Math.pow(this.x - v.getX(), 2) + Math.pow(this.y - v.getY(), 2));
        return  distance;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public float getTimeMin() {
        return timeMin;
    }

    public void setTimeMin(float timeMin) {
        this.timeMin = timeMin;
    }

    public float getTimeMax() {
        return timeMax;
    }

    public void setTimeMax(float timeMax) {
        this.timeMax = timeMax;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }
}
