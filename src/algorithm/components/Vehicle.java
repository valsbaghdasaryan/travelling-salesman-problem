package algorithm.components;

import algorithm.MetaHeuristics;

import java.util.ArrayList;

/**
 * Created by Vardan on 30/11/2016.
 */
public class Vehicle {

    private ArrayList<Vertex> vertices;
    private float distance;


    public Vehicle(Vertex v )
    {
        this.vertices = new ArrayList<>();
        this.vertices.add(v);
    }

    public Vehicle()
    {
        this.vertices = new ArrayList<>();
    }
    public Vehicle(ArrayList<Vertex> vertices)
    {
        this.vertices = vertices;
    }

    public void add(Vertex v)
    {
        this.vertices.add(v);
    }

    public void addAt(int index,Vertex v)
    {
        if(index >= this.vertices.size())
        {
            this.vertices.add(v);
        }
        else
        {
            this.vertices.add(index, v);
        }
    }

    public void addRandomly(Vertex v)
    {
        int index = (int ) (Math.random() * (this.vertices.size() + 1));
        addAt(index, v);
    }


    public Vertex removeRandom()
    {
        int index = (int)(Math.random() * vertices.size());
//        System.out.print(index +  ":" + vertices.size() + " ");
        return  remove(index);
    }

    public  Vertex remove(int index)
    {
        return vertices.remove(index);

    }

    public ArrayList<Vertex> getClonedVertices()
    {
        return Cloner.cloneVertices(this.vertices);
    }

    public Vertex remove(Vertex v)
    {
        int indexOf = vertices.indexOf(v);
        return remove(indexOf);
    }

    public float getDistance()
    {
        if(vertices.size() == 0)
        {
            return 0;
        }
        this.distance = MetaHeuristics.origin.getDistanceFrom(vertices.get(0));
        for( int i = 1 ; i < vertices.size(); i++)
        {
            this.distance += vertices.get(i-1).getDistanceFrom(vertices.get(i));
        }
        return this.distance;
    }

    public boolean isEmpty()
    {
        return (this.vertices.size() == 0);
    }

    public int numberOfVertices()
    {
        return this.vertices.size();
    }


    public void print() {
        System.out.print("Vehicle path: ");
        for(Vertex v : vertices)
        {
            System.out.print(v.getId() + "   ");
        }
        System.out.println(", distance: " + this.getDistance() + ":");
    }
}
