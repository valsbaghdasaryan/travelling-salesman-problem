package algorithm.components;

import java.io.*;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by Vardan on 30/11/2016.
 */
public class InputReader {

    private final static String PATH = "inputs/";
    private final static String ROUTAGE_A1 = "routage_a1.txt";
    private final static String TESTING = "testing.txt";
    private ArrayList<Vertex> vertices;

    private ArrayList<Vertex> getVertices()
    {
        return vertices;
    }


    public ArrayList<Vertex> readFromSource()
    {
        try{
//            File file = new File(PATH+TESTING);
            File file = new File(PATH+ROUTAGE_A1);
            FileReader reader = new FileReader(file);
            BufferedReader bReader = new BufferedReader(reader);
            String line;
            vertices = new ArrayList<>();

            bReader.readLine();// skip first  line
            bReader.readLine();// skip second line
            while((line = bReader.readLine()) != null)
            {
                String[] param = line.split("\t");
                addVertex(param);
            }

            reader.close();
            bReader.close();
            return vertices;
        }
        catch (IOException error)
        {
            error.printStackTrace();
        }
        return  null;
    }

    private void addVertex(String[] params)
    {
        Vertex v = new Vertex(
                Integer.parseInt(params[0]),// ID
                Float.parseFloat(params[1]),// x
                Float.parseFloat(params[2]),// y
                Float.parseFloat(params[3]),// quantity
                Float.parseFloat(params[4]),// tmin
                Float.parseFloat(params[5]),// tmax
                Float.parseFloat(params[6]) // duration
        );

        vertices.add(v);
    }

    public void print()
    {
        for(Vertex v : vertices)
        {
            v.print();
        }
    }
}
