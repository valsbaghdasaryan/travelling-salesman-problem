package algorithm.neighbors;

import algorithm.MetaHeuristics;
import algorithm.components.Solution;
import algorithm.components.Vehicle;
import algorithm.components.Vertex;

import java.util.ArrayList;

/**
 * Created by Vardan on 05/12/2016.
 */
public abstract class NeighborManager {
    protected ArrayList<Vertex> vertices;

    public NeighborManager(ArrayList<Vertex> vertices)
    {
        this.vertices = vertices;
    }

    public abstract Solution getNeighbor(Solution current);

/*
    */
/**
     * This method generate a sample of neighbors, keep the best one
     * @param current is the current solution, for which we have to find the neighbor solution
     * @return
     *//*

    public Solution getIntermediaryNeighbor(Solution current)
    {
        throw  new Error("function doesn't implemented ");
//        return  null;
    }

    */
/**
     * This method computes the first neighbor that is better than the current
     * solution (worse case : explore all neighbors)
     * @param current is the current solution, for which we have to find the neighbor solution
     * @return
     *//*

    public Solution getFirstBetterNeighbor(Solution current)
    {
        throw  new Error("function doesn't implemented ");
//        return  null;
    }

    */
/**
     * This method selects randomly a neighbor among the ones that are better
     * @param current is the current solution, for which we have to find the neighbor solution
     * @return
     *//*

    public Solution getRandomBetterNeighbor(Solution current)
    {
        throw  new Error("function doesn't implemented ");
//        return null;
    }
*/

}
