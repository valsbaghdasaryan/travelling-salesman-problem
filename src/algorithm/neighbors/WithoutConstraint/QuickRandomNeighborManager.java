package algorithm.neighbors.WithoutConstraint;

import algorithm.MetaHeuristics;
import algorithm.components.Solution;
import algorithm.components.Vehicle;
import algorithm.components.Vertex;
import algorithm.neighbors.NeighborManager;

import java.util.ArrayList;

/**
 * Created by Vardan on 08/12/2016.
 */
public class QuickRandomNeighborManager extends NeighborManager {

    public QuickRandomNeighborManager(ArrayList<Vertex> vertices)
    {
        super(vertices);
        System.out.println("Quick random neighbor manager");
    }

    /**
     * This method computes only one neighbor (not necessarily better)
     * @param current is the current solution, for which we have to find the neighbor solution
     * @return
     */
    @Override
    public Solution getNeighbor(Solution current)
    {
        ArrayList<Vehicle> currentVehicles = current.getVehiclesCloned();
        int vehicleNumber = (int)(Math.random() * currentVehicles.size());
        Vertex removedVertex = currentVehicles.get(vehicleNumber).removeRandom();
        if(currentVehicles.get(vehicleNumber).isEmpty())
        {
            currentVehicles.remove(vehicleNumber);
        }

        if(currentVehicles.size() < MetaHeuristics.verticesNumber)
        {
            vehicleNumber = (int)(Math.random() * (currentVehicles.size()+ 1));
            if(vehicleNumber == currentVehicles.size())
            {
                currentVehicles.add(new Vehicle());
            }
        }
        else {
            vehicleNumber = (int)(Math.random() * (currentVehicles.size()));
        }

        currentVehicles.get(vehicleNumber).addRandomly(removedVertex);

        return new Solution(currentVehicles);
    }
}
