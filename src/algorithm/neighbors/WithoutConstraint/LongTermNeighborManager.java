package algorithm.neighbors.WithoutConstraint;

import algorithm.components.Solution;
import algorithm.components.Vehicle;
import algorithm.components.Vertex;
import algorithm.neighbors.NeighborManager;

import java.util.ArrayList;

/**
 * Created by Vardan on 08/12/2016.
 */
public class LongTermNeighborManager extends NeighborManager {

    public LongTermNeighborManager(ArrayList<Vertex> vertices)
    {
        super(vertices);
        System.out.println("Long term (finding from all neighbor best) neighbor manager\n");
    }

    /**
     * This method enumerates all neighbors and keeps the best one
     * @param current is the current solution, for which we have to find the neighbor solution
     * @return
     */
    @Override
    public Solution getNeighbor(Solution current)
    {
        Solution best = current;
        for(int i = 0 ; i < current.getVehicles().size(); i++)// we chose 'i' vehicle
        {
            Vehicle currentVehicle = current.getVehicles().get(i);
            for(int j = 0 ; j < currentVehicle.numberOfVertices(); j++) // then we choose 'j' vertex of that vehicle
            {
                for(int k = 0; k < current.getVehicles().size(); k++) // we choose k vehicle to put already chosen vertex here
                {
                    for(int n = 0; n < currentVehicle.numberOfVertices(); n++) // then ww choose n place, to put it
                    {
                        /* no need to put the same place */
                        if(i == k && n == j)
                        {
                            continue;
                        }

                        /* generate the cloned array */
                        ArrayList<Vehicle> newVehicles = current.getVehiclesCloned();
                        /* then remove j vertex from i vehicle */
                        Vertex v = newVehicles.get(i).remove(j);
                        /* then add to k vehicle in n position */
                        newVehicles.get(k).addAt(n, v); //

                        // Check if the vehicle , from which we remove, is empty, delete that vehicle
                        if(newVehicles.get(i).isEmpty())
                        {
                            newVehicles.remove(i);
                        }

                        Solution s = new Solution(newVehicles);
                        if(s.isBetterThan(best))
                        {
                            best = s;
                        }
                    }
                }
                // we suppose that we can add another new vehicle

                 /* generate the cloned array */
                ArrayList<Vehicle> newVehicles = current.getVehiclesCloned();

                /* then remove j vertex from i vehicle */
                Vertex v = newVehicles.get(i).remove(j);

                /* we add new vehicle with only vertex "v" */
                newVehicles.add((new Vehicle(v)));


                Solution s = new Solution(newVehicles);
                if(s.isBetterThan(best))
                {
                    best = s;
                }
            }
        }
        return best;
    }
}
