package algorithm;

import algorithm.components.*;
import algorithm.neighbors.WithoutConstraint.LongTermNeighborManager;
import algorithm.neighbors.NeighborManager;
import algorithm.neighbors.WithoutConstraint.QuickRandomNeighborManager;
import algorithm.startSolutuions.WithoutConstraints.RandomStartSolutionManager;
import algorithm.startSolutuions.StartSolutionManager;

import java.util.ArrayList;

/**
 * Created by Vardan on 30/11/2016
 */
public class MetaHeuristics {

    private static int MAX_ITERATION_TIME = 100000;
    private static int MAX_NUMBER_OF_ATTEMPTS = 100;

    public static Vertex origin;
    public static int verticesNumber;

    private ArrayList<Vertex> vertices;


    public MetaHeuristics()
    {
        this.init();

        this.startLocalSearch1();
        this.startLocalSearch2();
        this.startLocalSearchWithALotAttempts();
    }

    private void init()
    {
        this.vertices   = new InputReader().readFromSource();
        this.origin     = this.vertices.remove(0);// TODO need to check, if need to remove

        MetaHeuristics.verticesNumber = this.vertices.size();
        MetaHeuristics.MAX_ITERATION_TIME = this.vertices.size() * this.vertices.size() * 10;
    }


    private void startLocalSearch1()
    {
        System.out.println("__________________________________Local search 1 ________________________________ \n");
        int iterationTime = 0;
        double startTime = System.currentTimeMillis();
        StartSolutionManager randomStart = new RandomStartSolutionManager(this.vertices);
        Solution currentSolution = randomStart.generate();

        NeighborManager neighborManager = new QuickRandomNeighborManager(this.vertices);

        System.out.println("Start solution is :");
        currentSolution.print();


        while (!currentSolution.isAcceptableSolution() && iterationTime < MAX_ITERATION_TIME)
        {
            Solution newSolution = neighborManager.getNeighbor(currentSolution);
            if(newSolution.isBetterThan(currentSolution))
            {
                currentSolution = newSolution;
            }
            iterationTime ++;
        }

        currentSolution.print();
        this.printTimePast(startTime);
        System.out.println("__________________________________ final solution ________________________________ ");

        System.out.println("\n\n");
        System.out.println("\n\n");
    }

    private void printTimePast(double startTime) {
        System.out.println("Execution time: " + (System.currentTimeMillis() - startTime));

    }

    private void startLocalSearch2()
    {
        System.out.println("__________________________________Local search 2 ________________________________ \n");
        int iterationTime = 0;
        double startTime = System.currentTimeMillis();
        StartSolutionManager randomStart = new RandomStartSolutionManager(this.vertices);
        Solution currentSolution = randomStart.generate();

        NeighborManager neighborManager = new LongTermNeighborManager(this.vertices);

        System.out.println(" Start solution is :");
        currentSolution.print();

        while (!currentSolution.isAcceptableSolution() && iterationTime < MAX_ITERATION_TIME)
        {
            Solution newSolution = neighborManager.getNeighbor(currentSolution);
            if(newSolution == currentSolution)
            {
                currentSolution = newSolution;
                break;
            }
            currentSolution = newSolution;
            iterationTime ++;
        }

        currentSolution.print();
        this.printTimePast(startTime);
        System.out.println("__________________________________ final solution ________________________________ ");
        System.out.println("\n\n");
        System.out.println("\n\n");
    }

    private void startLocalSearchWithALotAttempts()
    {
        System.out.println("_______ start local search with different start solutions (with " + MAX_NUMBER_OF_ATTEMPTS + " attempts)________________________________ \n");
        int iterationTime = 0;
        int numberOfAttempts = 0;
        double startTime = System.currentTimeMillis();
        StartSolutionManager randomStart = new RandomStartSolutionManager(this.vertices);


        Solution global = null;
        Solution local;

        NeighborManager neighborManager = new QuickRandomNeighborManager(this.vertices);

        while(numberOfAttempts < MAX_NUMBER_OF_ATTEMPTS)
        {

            local = randomStart.generate();
            iterationTime = 0;
            while (!local.isAcceptableSolution() && iterationTime < MAX_ITERATION_TIME)
            {
                Solution newSolution = neighborManager.getNeighbor(local);
                if(newSolution.isBetterThan(local))
                {
                    local = newSolution;
                }
                iterationTime ++;
            }
            if(global == null)
            {
                global = local;
            }
            else {
                if(local.isBetterThan(global))
                {
                    global = local;
                }
            }

            numberOfAttempts ++;
        }
        global.print();
        this.printTimePast(startTime);
        System.out.println("__________________________________ final solution ________________________________ ");
        System.out.println("\n\n");
        System.out.println("\n\n");
    }
}
