package algorithm.startSolutuions.WithoutConstraints;

import algorithm.components.Solution;
import algorithm.components.Vehicle;
import algorithm.components.Vertex;
import algorithm.startSolutuions.StartSolutionManager;

import java.util.ArrayList;

/**
 * Created by Vardan on 08/12/2016.
 */
public class OneVehicleStartSolution extends StartSolutionManager {
    public OneVehicleStartSolution(ArrayList<Vertex> vertices)
    {
        super(vertices);
    }

    @Override
    public Solution generate() {
        System.out.println("Generate solution with one vehicle");
        ArrayList<Vehicle> routes = new ArrayList<>();
        Vehicle r = new Vehicle();
        for( Vertex v : this.vertices)
        {
            r.add(v);
        }
        routes.add(r);

        return  new Solution(routes);
    }
}
