package algorithm.startSolutuions.WithoutConstraints;

import algorithm.components.Cloner;
import algorithm.components.Solution;
import algorithm.components.Vehicle;
import algorithm.components.Vertex;
import algorithm.startSolutuions.StartSolutionManager;

import java.util.ArrayList;

/**
 * Created by Vardan on 08/12/2016.
 */
public class RandomStartSolutionManager extends StartSolutionManager {
    public RandomStartSolutionManager(ArrayList<Vertex> vertices)
    {
        super(vertices);
        System.out.println("Generate random start solution");
    }

    @Override
    public Solution generate() {

        ArrayList<Vertex> cloned = Cloner.cloneVertices(this.vertices);
        ArrayList<Vehicle> vehicles = new ArrayList<>();

        while (cloned.size() > 0)
        {
            int verticesLeft = getRandom(1, cloned.size() );
            Vehicle currentVehicle = new Vehicle();
            vehicles.add(currentVehicle);
            while (verticesLeft > 0)
            {
                int index = getRandom(0, cloned.size());
                currentVehicle.add(cloned.remove(index));
                verticesLeft --;
            }
        }

        return new Solution(vehicles);

    }
}
