package algorithm.startSolutuions;

import algorithm.components.Cloner;
import algorithm.components.Vehicle;
import algorithm.components.Solution;
import algorithm.components.Vertex;

import java.util.ArrayList;

/**
 * Created by Vardan on 05/12/2016.
 */
public abstract class StartSolutionManager {

    protected ArrayList<Vertex> vertices;

    public StartSolutionManager(ArrayList<Vertex> vertices)
    {
        this.vertices = vertices;
    }

    public abstract Solution generate();

    protected int getRandom(int from, int to)
    {
        if(from == to)
            return from;
        return (int) (Math.random() * (to - from)) + from;
    }
}
