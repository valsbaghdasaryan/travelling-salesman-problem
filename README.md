# Travelling salesman problem
The Travelling Salesman Problem (often called TSP) is a classic algorithmic problem in the field of computer science and operations research[1]. 
It is focused on optimization. In this context better solution often means a solution that is cheaper.
TSP is a mathematical problem. It is most easily expressed as a graph describing the locations of a set of nodes.

https://simple.wikipedia.org/wiki/Travelling_salesman_problem